# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand
from django.contrib.sessions.models import Session
from django.utils import timezone
from accounts.models import Account


class Command(BaseCommand):
    help = 'Reload "current_account" in user sessions. Use after altering (migrating) Account model.'

    def handle(self, *args, **options):
        self.stdout.write('Start reloading accounts ...')
        active_sessions = Session.objects.filter(expire_date__gt=timezone.now())
        for session in active_sessions:
            data = session.get_decoded()
            if 'current_account' in data:
                try:
                    account = Account.objects.get(pk=data['current_account'].pk)
                    data['current_account'] = account
                    session.session_data = Session.objects.encode(data)
                    session.save()
                except ObjectDoesNotExist:
                    pass
        self.stdout.write('Complete reloading accounts ...')
