# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.flatpages.views import flatpage
from django.views.generic import TemplateView

from dajaxice.core import dajaxice_autodiscover, dajaxice_config
from django.views.i18n import javascript_catalog

from .views import ContactView, PricingView, DomainView
from accounts.views import (AccountSettingsView, MembersView, GuestsView, MemberCreateView, GuestCreateView,
                            ExportMembersPdfView, ExportMembersXlsView, AssistantCreateView)
from documents.views import RootFolderRedirectView

admin.autodiscover()
dajaxice_autodiscover()


js_info_dict = {'packages': ('common',)}

urlpatterns = patterns(
    '',
    url(r'^privacy/$', flatpage, {'url': '/privacy/'}, name='privacy'),
    url(r'^terms/$', flatpage, {'url': '/terms/'}, name='terms'),
)

urlpatterns += patterns(
    'common.views',
    url(r'^', include('registration.backends.default.urls')),
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
    url(r'^jsi18n/$', javascript_catalog, js_info_dict),
    url(r'^mEOu0wA.html$', DomainView.as_view()),

    url(r'^$', 'main', name='main'),
    url(r'^app-admin/', include(admin.site.urls)),
    url(r'^accounts/', include('accounts.urls', 'accounts')),
    url(r'^accounts/email/', include('change_email.urls')),
    url(r'^billing/', include('billing.urls', 'billing')),
    url(r'^profile/', include('profiles.urls', 'profiles')),
    url(r'^contact/thankyou/', TemplateView.as_view(template_name='thankyou.html'), name='thankyou'),
    url(r'^contact/', ContactView.as_view(), name='contactus'),
    url(r'^documents/', include('documents.urls.document', 'documents')),
    url(r'^pricing/', PricingView.as_view(), name='pricing'),
    url(r'^pricing-frame/', PricingView.as_view(template_name="pricing_frame.html"), name='pricing-frame'),

    url(r'^(?P<url>[^/]+)/$', AccountSettingsView.as_view(), name='board_detail'),
    url(r'^(?P<url>[^/]+)/archives/', RootFolderRedirectView.as_view()),
    url(r'^(?P<url>[^/]+)/folders/', include('documents.urls.folder', 'folders')),
    url(r'^(?P<url>[^/]+)/committees/', include('committees.urls', 'committees')),
    url(r'^(?P<url>[^/]+)/news/', include('news.urls', 'news')),
    url(r'^(?P<url>[^/]+)/calendar/', include('boardcalendar.urls', 'calendar')),
    url(r'^(?P<url>[^/]+)/dashboard/', include('dashboard.urls', 'dashboard')),
    url(r'^(?P<url>[^/]+)/rsvp/', include('rsvp.urls', 'rsvp')),
    url(r'^(?P<url>[^/]+)/meetings/', include('meetings.urls', 'meetings')),
    url(r'^(?P<url>[^/]+)/events/', include('meetings.events_urls', 'events')),
    url(r'^(?P<url>[^/]+)/members/$', MembersView.as_view(), name='board_members'),
    url(r'^(?P<url>[^/]+)/guests/$', GuestsView.as_view(), name='board_guests'),
    url(r'^(?P<url>[^/]+)/members/create/$', MemberCreateView.as_view(), name='member_create'),
    url(r'^(?P<url>[^/]+)/guests/create/$', GuestCreateView.as_view(), name='guest_create'),
    url(r'^(?P<url>[^/]+)/assistant/(?P<pk>\d+)/create/$', AssistantCreateView.as_view(), name='assistant_create'),
    url(r'^(?P<url>[^/]+)/export/pdf/$', ExportMembersPdfView.as_view(), name='export-pdf'),
    url(r'^(?P<url>[^/]+)/export/xls/$', ExportMembersXlsView.as_view(), name='export-xls'),
)


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
