$(document).ready(function() {
    $('#browse-by').on('change', function() {
        var committee_id = $(this).val();
        Dajaxice.common.committees_filter(Dajax.process, {'committee_id': committee_id});
    });
});

function update_committee_list(data){
    $('#committees').children().fadeOut(500, function () {
        $('#committees').children().remove();
        $(data.committees).appendTo('#committees').hide().fadeIn(500);
    });
    $('#mail-to').attr('href', data.members_email);
}
