$(document).ready(function(){
    var inputAddress = $('#location');
    var geocoder = new google.maps.Geocoder();

    function makeMapLink() {
        geocoder.geocode({'address' : inputAddress.text()}, function(result, status){
            if (result.length > 0) {
                var location = result[0].geometry.location;
                var url = 'https://www.google.com/maps/?z=15&q=loc:' + location.lat() + ',' + location.lng();
                console.log("Geocoder result", result, url);

                $('.location-link').attr('href', url).show();
            }
        });
    }

    makeMapLink();
});
