# -*- coding: utf-8 -*-


class FormValidMixin(object):
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.account = self.request.session['current_account']
        self.object.created_member = self.request.user.get_membership(self.object.account)
        self.object.save()
        self.get_success_message()
        return super(FormValidMixin, self).form_valid(form)
