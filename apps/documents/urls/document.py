# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from documents.views import (DocumentAjaxCreateView, DocumentAjaxDeleteView,
                             DocumentDownloadView, DocumentRevisionDownloadView, DocumentSendView)
from documents.views.document import DocumentMoveView

urlpatterns = patterns(
    '',
    url(r'^create/$', DocumentAjaxCreateView.as_view(), name='create'),
    url(r'^delete/$', DocumentAjaxDeleteView.as_view(), name='delete'),
    url(r'^download/(?P<document_id>\d+)/$', DocumentDownloadView.as_view(), name='download'),
    url(r'^download/(?P<document_id>\d+)/v(?P<revision>\d+)$', DocumentRevisionDownloadView.as_view(), name='download-revision'),
    url(r'^send/(?P<document_id>\d+)/$', DocumentSendView.as_view(), name='send'),
    url(r'^move/(?P<document_id>\d+)/$', DocumentMoveView.as_view(), name='move'),
)
