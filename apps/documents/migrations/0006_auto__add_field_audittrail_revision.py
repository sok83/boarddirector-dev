# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'AuditTrail.revision'
        db.add_column(u'documents_audittrail', 'revision',
                      self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'AuditTrail.revision'
        db.delete_column(u'documents_audittrail', 'revision')


    models = {
        u'accounts.account': {
            'Meta': {'object_name': 'Account'},
            'date_cancel': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'plan': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Plan']", 'null': 'True'}),
            'send_notification': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'stripe_customer_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'total_storage': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'}),
            'view_email': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'billing.plan': {
            'Meta': {'ordering': "['month_price']", 'object_name': 'Plan'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_members': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'max_storage': ('billing.fields.BigIntegerSizeField', [], {}),
            'month_price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2', 'unique': 'True'}),
            'stripe_month_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'stripe_year_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'year_price': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'committees.committee': {
            'Meta': {'ordering': "['name']", 'object_name': 'Committee'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'committees'", 'to': u"orm['accounts.Account']"}),
            'chairman': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['profiles.Membership']", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'})
        },
        u'documents.audittrail': {
            'Meta': {'object_name': 'AuditTrail'},
            'change_type': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 3, 10, 0, 0)'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['documents.GroupDocument']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latest_version': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'meeting': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'audits'", 'null': 'True', 'to': u"orm['meetings.Meeting']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'revision': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.PositiveIntegerField', [], {'default': '3'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'audits'", 'to': u"orm['profiles.User']"})
        },
        u'documents.document': {
            'Meta': {'object_name': 'Document'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'documents'", 'null': 'True', 'to': u"orm['accounts.Account']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 3, 10, 0, 0)'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['documents.GroupDocument']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meeting': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'documents'", 'null': 'True', 'to': u"orm['meetings.Meeting']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'previous_version': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.PositiveIntegerField', [], {'default': '3'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'documents'", 'to': u"orm['profiles.User']"})
        },
        u'documents.groupdocument': {
            'Meta': {'unique_together': "(('name', 'category'),)", 'object_name': 'GroupDocument'},
            'category': ('django.db.models.fields.SmallIntegerField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'meetings.meeting': {
            'Meta': {'ordering': "['-start']", 'object_name': 'Meeting'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'meetings'", 'to': u"orm['accounts.Account']"}),
            'committee': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'meetings'", 'null': 'True', 'to': u"orm['committees.Committee']"}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'profiles.membership': {
            'Meta': {'unique_together': "(('user', 'account'),)", 'object_name': 'Membership'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'memberships'", 'to': u"orm['accounts.Account']"}),
            'affiliation': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'assistant': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'assistants'", 'null': 'True', 'to': u"orm['profiles.Membership']"}),
            'avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'bio': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'committees': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'memberships'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['committees.Committee']"}),
            'crops': ('croppy.fields.CropField', [], {'default': '{}', 'image_field': "'avatar'"}),
            'date_joined_board': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'employer': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intro': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'job_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'linkedin': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'role': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'term_expires': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'term_start': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'timezone': ('timezone_field.fields.TimeZoneField', [], {'default': "'America/Chicago'"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profiles.User']"})
        },
        u'profiles.user': {
            'Meta': {'object_name': 'User'},
            'accounts': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'users'", 'null': 'True', 'through': u"orm['profiles.Membership']", 'to': u"orm['accounts.Account']"}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['documents']