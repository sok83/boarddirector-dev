# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from meetings.urls import make_meeting_urls
from meetings.views import MeetingDetailView, MeetingCreateView, MeetingsView, MeetingUpdateView, MeetingDeleteView, PastMeetingsView, MeetingMailView

urlpatterns = make_meeting_urls(kwargs={'type': 'event'})