# -*- coding: utf-8 -*-
from django.views.generic.list import ListView

from common.mixins import ActiveTabMixin, SelectBoardRequiredMixin
from meetings.models import Meeting
from permissions import PERMISSIONS
from permissions.mixins import PermissionMixin


class CalendarView(ActiveTabMixin, SelectBoardRequiredMixin, PermissionMixin, ListView):
    permission = (Meeting, PERMISSIONS.view)
    context_object_name = 'meetings'
    template_name = 'calendar/calendar_list.html'
    active_tab = 'calendar'

    def get_queryset(self):
        membership = self.request.user.get_membership(self.request.session['current_account'])
        queryset = Meeting.objects.for_membership(membership, only_own_meetings=True)
        queryset = queryset.filter(status=Meeting.STATUSES.published)
        meetings_list = queryset.distinct()
        return meetings_list
