# -*- coding: utf-8 -*-
from django.db import models
from django import forms

from south.modelsinspector import add_introspection_rules


class BigIntegerSizeFormField(forms.IntegerField):

    def to_python(self, value):
        value = super(BigIntegerSizeFormField, self).to_python(value)
        value *= 1024 ** 3
        return value

    def prepare_value(self, value):
        value = super(BigIntegerSizeFormField, self).prepare_value(value)
        value /= 1024 ** 3
        return value


class BigIntegerSizeField(models.BigIntegerField):
    def formfield(self, **kwargs):
        defaults = {
            'min_value': -BigIntegerSizeField.MAX_BIGINT - 1,
            'max_value': BigIntegerSizeField.MAX_BIGINT,
            'form_class': BigIntegerSizeFormField,
        }
        defaults.update(kwargs)
        return super(BigIntegerSizeField, self).formfield(**defaults)

add_introspection_rules([], ["^billing\.fields\.BigIntegerSizeField"])
