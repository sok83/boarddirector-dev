# -*- coding: utf-8 -*-
from south.db import db
from south.v2 import DataMigration

from billing.models import Plan


class Migration(DataMigration):

    plans_2016 = [
        Plan(name=Plan.STARTER_2016,
             max_members = 9,
             max_storage = 53687091200,
             month_price = 49,
             year_price = 499,
             stripe_month_plan_id = "starter_month",
             stripe_year_plan_id = "starter_year",
             available=True),
        Plan(name=Plan.STANDARD_2016,
             max_members = 19,
             max_storage = 53687091200,
             month_price = 99,
             year_price = 999,
             stripe_month_plan_id = "standard_month",
             stripe_year_plan_id = "standard_year",
             available=True),
        Plan(name=Plan.PREMIER_2016,
             max_members = 0,
             max_storage = 0,
             month_price = 149,
             year_price = 1499,
             stripe_month_plan_id = "premier_month",
             stripe_year_plan_id = "premier_year",
             available=True)
    ]

    def forwards(self, orm):
        # Add the 2016 plans
        for p in self.plans_2016:
            db_records = Plan.objects.filter(name=p.name)
            if db_records.exists():
                rec = db_records[0]
                rec.max_members = p.max_members
                rec.max_storage = p.max_storage
                rec.month_price = p.month_price
                rec.year_price = p.year_price
                rec.stripe_month_plan_id = p.stripe_month_plan_id
                rec.stripe_year_plan_id = p.stripe_year_plan_id
                rec.available = p.available
                rec.save()
            else:
                p.save()


    def backwards(self, orm):
        # Delete the 2016 plans
        Plan.objects.filter(name__in=[p.name for p in self.plans_2016]).delete()

    models = {
        u'accounts.account': {
            'Meta': {'object_name': 'Account'},
            'date_cancel': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'plan': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Plan']", 'null': 'True'}),
            'send_notification': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'stripe_customer_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'total_storage': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'}),
            'view_email': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'billing.billingsettings': {
            'Meta': {'object_name': 'BillingSettings'},
            'account': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'billing_settings'", 'unique': 'True', 'to': u"orm['accounts.Account']"}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'card_number': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'cvv': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'cycle': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'discount': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'expiration_month': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'expiration_year': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'zip': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'billing.invoice': {
            'Meta': {'object_name': 'Invoice'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'invoices'", 'to': u"orm['accounts.Account']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'payed_period_end': ('django.db.models.fields.DateTimeField', [], {}),
            'payment': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2'})
        },
        u'billing.plan': {
            'Meta': {'ordering': "['month_price']", 'object_name': 'Plan'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_members': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'max_storage': ('billing.fields.BigIntegerSizeField', [], {}),
            'month_price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2', 'unique': 'True'}),
            'stripe_month_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'stripe_year_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'year_price': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['billing']