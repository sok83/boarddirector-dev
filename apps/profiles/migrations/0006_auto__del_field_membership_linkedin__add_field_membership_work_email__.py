# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Membership.linkedin'
        db.delete_column(u'profiles_membership', 'linkedin')

        # Adding field 'Membership.work_email'
        db.add_column(u'profiles_membership', 'work_email',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'Membership.work_number'
        db.add_column(u'profiles_membership', 'work_number',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'Membership.address'
        db.add_column(u'profiles_membership', 'address',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'Membership.secondary_address'
        db.add_column(u'profiles_membership', 'secondary_address',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'Membership.city'
        db.add_column(u'profiles_membership', 'city',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50, blank=True),
                      keep_default=False)

        # Adding field 'Membership.state'
        db.add_column(u'profiles_membership', 'state',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50, blank=True),
                      keep_default=False)

        # Adding field 'Membership.zip'
        db.add_column(u'profiles_membership', 'zip',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50, blank=True),
                      keep_default=False)

        # Adding field 'Membership.country'
        db.add_column(u'profiles_membership', 'country',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50, blank=True),
                      keep_default=False)

        # Adding field 'Membership.birth_date'
        db.add_column(u'profiles_membership', 'birth_date',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Membership.secondary_phone'
        db.add_column(u'profiles_membership', 'secondary_phone',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'Membership.social_media_link'
        db.add_column(u'profiles_membership', 'social_media_link',
                      self.gf('django.db.models.fields.URLField')(default='', max_length=255, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Membership.linkedin'
        db.add_column(u'profiles_membership', 'linkedin',
                      self.gf('django.db.models.fields.URLField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Deleting field 'Membership.work_email'
        db.delete_column(u'profiles_membership', 'work_email')

        # Deleting field 'Membership.work_number'
        db.delete_column(u'profiles_membership', 'work_number')

        # Deleting field 'Membership.address'
        db.delete_column(u'profiles_membership', 'address')

        # Deleting field 'Membership.secondary_address'
        db.delete_column(u'profiles_membership', 'secondary_address')

        # Deleting field 'Membership.city'
        db.delete_column(u'profiles_membership', 'city')

        # Deleting field 'Membership.state'
        db.delete_column(u'profiles_membership', 'state')

        # Deleting field 'Membership.zip'
        db.delete_column(u'profiles_membership', 'zip')

        # Deleting field 'Membership.country'
        db.delete_column(u'profiles_membership', 'country')

        # Deleting field 'Membership.birth_date'
        db.delete_column(u'profiles_membership', 'birth_date')

        # Deleting field 'Membership.secondary_phone'
        db.delete_column(u'profiles_membership', 'secondary_phone')

        # Deleting field 'Membership.social_media_link'
        db.delete_column(u'profiles_membership', 'social_media_link')


    models = {
        u'accounts.account': {
            'Meta': {'object_name': 'Account'},
            'date_cancel': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'plan': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Plan']", 'null': 'True'}),
            'send_notification': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'show_guide': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'stripe_customer_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'total_storage': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'}),
            'view_email': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'billing.plan': {
            'Meta': {'ordering': "['month_price']", 'object_name': 'Plan'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_members': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'max_storage': ('billing.fields.BigIntegerSizeField', [], {}),
            'month_price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2', 'unique': 'True'}),
            'stripe_month_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'stripe_year_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'year_price': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'committees.committee': {
            'Meta': {'ordering': "['name']", 'object_name': 'Committee'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'committees'", 'to': u"orm['accounts.Account']"}),
            'chairman': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['profiles.Membership']", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'})
        },
        u'profiles.membership': {
            'Meta': {'unique_together': "(('user', 'account'),)", 'object_name': 'Membership'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'memberships'", 'to': u"orm['accounts.Account']"}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'affiliation': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'assistant': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'bosses'", 'null': 'True', 'to': u"orm['profiles.Membership']"}),
            'avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'bio': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'birth_date': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'committees': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'memberships'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['committees.Committee']"}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'crops': ('croppy.fields.CropField', [], {'default': '{}', 'image_field': "'avatar'"}),
            'date_joined_board': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'employer': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intro': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'invitation_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'job_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'role': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'secondary_address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'secondary_phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'social_media_link': ('django.db.models.fields.URLField', [], {'max_length': '255', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'term_expires': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'term_start': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'timezone': ('timezone_field.fields.TimeZoneField', [], {'default': "'America/Chicago'"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profiles.User']"}),
            'work_email': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'work_number': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        u'profiles.temporaryuserpassword': {
            'Meta': {'object_name': 'TemporaryUserPassword'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'tmppswd'", 'unique': 'True', 'to': u"orm['profiles.User']"})
        },
        u'profiles.user': {
            'Meta': {'object_name': 'User'},
            'accounts': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'users'", 'null': 'True', 'through': u"orm['profiles.Membership']", 'to': u"orm['accounts.Account']"}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['profiles']