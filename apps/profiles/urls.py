# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from .views import EditProfileView, MemberView, MembershipDeleteView, InviteMemberView, AssistantView, EditAssistantView, \
    AssistantDeleteView


urlpatterns = patterns(
    'profiles.views',
    url(r'^edit/(?P<pk>\d+)/$', EditProfileView.as_view(), name='edit'),
    url(r'^delete/(?P<pk>\d+)/$', MembershipDeleteView.as_view(), name='delete'),
    url(r'^edit/(?P<member_pk>\d+)/(?P<pk>\d+)/$', EditAssistantView.as_view(), name='assistant_edit'),
    url(r'^delete/(?P<member_pk>\d+)/(?P<pk>\d+)/$', AssistantDeleteView.as_view(), name='assistant_delete'),
    url(r'^(?P<pk>\d+)/$', MemberView.as_view(), name='detail'),
    url(r'^(?P<member_pk>\d+)/(?P<pk>\d+)/assistant/$', AssistantView.as_view(), name='assistant_detail'),
    url(r'^invite/(?P<user_pk>\d+)/$', InviteMemberView.as_view(), name='invite'),
    url(r'^$', MemberView.as_view(), name='detail'),
)
