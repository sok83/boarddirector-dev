# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.utils.decorators import method_decorator
from permissions.shortcuts import has_role_permission, has_object_permission


class PermissionMixin(object):
    def and_permission(self, account, membership):
        return True

    def or_permission(self, account, membership):
        return False

    def get_permission_object(self):
        try:
            return self.get_object()
        except:
            return None

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        account = request.session['current_account']
        membership = request.user.get_membership(account)
        model, permission = self.permission
        obj = self.get_permission_object()
        if (self.and_permission(account, membership) and
            ((has_role_permission(membership, model, permission) or has_object_permission(membership, obj, permission)) or
             self.or_permission(account, membership))):
            return super(PermissionMixin, self).dispatch(request, *args, **kwargs)
        raise PermissionDenied()
